<?php

namespace Drupal\gated_entity\Plugin;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;

/**
 * Provides the Gated Entity Locker plugin manager.
 */
class GatedEntityLockerManager extends DefaultPluginManager {

  /**
   * Constructs a new GatedEntityLockerManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct('Plugin/GatedEntityLocker', $namespaces, $module_handler, 'Drupal\gated_entity\Plugin\GatedEntityLockerInterface', 'Drupal\gated_entity\Annotation\GatedEntityLocker');
    $this->alterInfo('gated_entity_locker_info');
    $this->setCacheBackend($cache_backend, 'gated_entity_locker_plugins');
  }

}
