<?php

namespace Drupal\gated_entity\Plugin;

use Drupal\Component\Plugin\PluginBase;
use Drupal\Core\Form\FormState;

/**
 * Base class for gated entity locker plugins.
 */
abstract class GatedEntityLockerBase extends PluginBase implements GatedEntityLockerInterface {

  /**
   * {@inheritdoc}
   */
  public function getForm() {
    if (isset($this->pluginDefinition['form_class']) && $this->pluginDefinition['form_class']) {
      $formState = new FormState();
      return \Drupal::formBuilder()->buildForm($this->pluginDefinition['form_class'], $formState);
    }
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function buildLocker() {
    if ($form = $this->getForm()) {
      return $form;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getLabel() {
    return $this->pluginDefinition['label'];
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->pluginDefinition['description'];
  }

}
