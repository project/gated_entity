<?php

namespace Drupal\gated_entity\Plugin;

use Drupal\Component\Plugin\PluginInspectionInterface;

/**
 * Defines an interface for Gated entity locker plugins.
 */
interface GatedEntityLockerInterface extends PluginInspectionInterface {

  /**
   * Returns the render-able locker for Gated Entity.
   *
   * @return array
   *   The render-able array.
   */
  public function buildLocker();

  /**
   * Returns TRUE if the access is allowed, otherwise FALSE.
   *
   * @return bool
   *   The access state.
   */
  public function checkAccess();

  /**
   * Returns the form referenced in this plugin.
   *
   * @return \Drupal\Core\Form\FormBase
   *   The associated form.
   */
  public function getForm();

  /**
   * Returns the administrative label for this plugin.
   *
   * @return string
   *   The label of the plugin.
   */
  public function getLabel();

  /**
   * Returns the administrative description for this plugin.
   *
   * @return string
   *   The description fof the plugin.
   */
  public function getDescription();

}
