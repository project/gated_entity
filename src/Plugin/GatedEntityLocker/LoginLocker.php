<?php

namespace Drupal\gated_entity\Plugin\GatedEntityLocker;

use Drupal\Core\Link;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;
use Drupal\gated_entity\Plugin\GatedEntityLockerBase;

/**
 * Provides Login link plugin for gated entity locker.
 *
 * @GatedEntityLocker(
 *   id = "login_locker",
 *   label = @Translation("Login to unlock"),
 *   form_class = "\Drupal\gated_entity\Form\PasswordLockerForm"
 * )
 */
class LoginLocker extends GatedEntityLockerBase {

  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public function buildLocker() {
    // @todo Make use of DI.
    $url = Url::fromRoute('user.login', ['destination' => \Drupal::service('path.current')->getPath()]);
    $link = Link::fromTextAndUrl($this->t('Login to unlock'), $url);
    $build = ['login_link' => $link->toRenderable()];
    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function checkAccess() {
    // @todo Make use of DI.
    if (\Drupal::currentUser()->isAuthenticated()) {
      return TRUE;
    }
    return FALSE;
  }

}
