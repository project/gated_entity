<?php

namespace Drupal\gated_entity\Form;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\gated_entity\GatedEntityHelper;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides configuration form for gated entities.
 */
class GatedEntityConfigForm extends ConfigFormBase {

  /**
   * The gated entity helper.
   *
   * @var \Drupal\gated_entity\GatedEntityHelper
   */
  protected $gatedEntityHelper;

  /**
   * Configuration name for gated entity config.
   *
   * @const string
   */
  protected const GATED_ENTITY_CONFIG = 'gated_entity.config';

  /**
   * The allowed entity types.
   *
   * @var array
   */
  protected $allowedTypes;

  /**
   * Constructs a GatedEntityConfigForm object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\gated_entity\GatedEntityHelper $gated_entity_helper
   *   The gated entity helper.
   */
  public function __construct(ConfigFactoryInterface $config_factory, GatedEntityHelper $gated_entity_helper) {
    parent::__construct($config_factory);
    $this->gatedEntityHelper = $gated_entity_helper;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('gated_entity.helper')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'gated_entity_config_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [self::GATED_ENTITY_CONFIG];
  }

  /**
   * {@inheritdoc}
   *
   * @param array $form
   *   A nested array form elements comprising the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config(self::GATED_ENTITY_CONFIG);
    // Section for configuring entitiess.
    $form['gated_entity_config'] = [
      '#type' => 'details',
      '#title' => $this->t('Configure Entities'),
      '#open' => TRUE,
    ];
    // Get allowed entity types to populate in the form.
    $this->allowedTypes = $this->gatedEntityHelper->getAllowedEntityTypes();
    foreach ($this->allowedTypes as $type => $name) {
      $instances = [];
      $form['gated_entity_config']['entity' . $type] = [
        '#type' => 'details',
        '#title' => $name['name'],
      ];
      $instances = $this->gatedEntityHelper->getConfigurableEntities($type);
      $form['gated_entity_config']['entity' . $type][$type] = [
        '#title' => $this->t('Entities'),
        '#type' => 'checkboxes',
        '#options' => $instances,
        '#default_value' => $config->get('entities')[$type] ?? [],
      ];
    }
    // Section for configuring locker.
    $form['gated_entity_locker_settings'] = [
      '#type' => 'details',
      '#title' => $this->t('Configure Locker'),
      '#open' => TRUE,
    ];
    // Get all the available lockers and populate.
    $lockers = $this->gatedEntityHelper->getLockers();
    $form['gated_entity_locker_settings']['default_locker'] = [
      '#type' => 'radios',
      '#options' => $lockers,
      '#title' => $this->t("Default Locker"),
      '#default_value' => $this->gatedEntityHelper->getDefaultLockerId(),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    $values = $form_state->getValues();
    $types = [];
    foreach (array_keys($this->allowedTypes) as $type) {
      if (isset($values[$type])) {
        $types[$type] = array_filter($values[$type]);
      }
    }
    Cache::invalidateTags(['node_view']);
    $this->config(self::GATED_ENTITY_CONFIG)
      ->set('entities', $types)
      ->set('default_locker', $values['default_locker'])
      ->save();
  }

}
