<?php

namespace Drupal\gated_entity\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a Gated Entity Locker item annotation object.
 *
 * @see \Drupal\gated_entity\Plugin\GatedEntityLockerManager
 * @see plugin_api
 *
 * @Annotation
 */
class GatedEntityLocker extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The label of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $label;

  /**
   * The description of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $description;

  /**
   * The class used to represent the form.
   *
   * @var string
   */
  public $form_class = NULL;

}
