<?php

namespace Drupal\gated_entity;

use Drupal\Core\Security\TrustedCallbackInterface;

/**
 * Provides post render callback support.
 */
class GatedEntityCallback implements TrustedCallbackInterface {

  /**
   * Post render callback for gated entity.
   *
   * @param string $content
   *   Rendered content.
   * @param array $element
   *   Render array.
   *
   * @return string
   *   Rendered content.
   */
  public static function postRender($content, array $element) {
    // @todo Make it compatible with all the entities.
    // For now, only nodes are allowed so it is okay.
    $entity = $element['#node'];
    if ($locker = \Drupal::service('gated_entity.helper')->getLockerByEntity($entity)) {
      $build = [];
      $build['title'] = isset($element['#title']) ? ['#markup' => $element['#title']] : $element['title'];
      $build[$locker->getPluginId()] = $locker->buildLocker();
      $content = \Drupal::service('renderer')->render($build);
    }
    return $content;
  }

  /**
   * {@inheritdoc}
   */
  public static function trustedCallbacks() {
    return ['postRender'];
  }

}
