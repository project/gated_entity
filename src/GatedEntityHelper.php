<?php

namespace Drupal\gated_entity;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\gated_entity\Plugin\GatedEntityLockerManager;

/**
 * Gated entity helper service.
 */
class GatedEntityHelper {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Gated entity locker manager.
   *
   * @var \Drupal\gated_entity\Plugin\GatedEntityLockerManager
   */
  protected $lockerManager;

  /**
   * ALlowed entity types.
   */
  protected const ALLOWED_ENTITY_TYPES = [
    'node_type' => 'node',
  ];

  /**
   * Default locker.
   */
  protected const DEFAULT_LOCKER_ID = 'login_locker';

  /**
   * Configuration name for gated entity config.
   *
   * @const string
   */
  protected const GATED_ENTITY_CONFIG = 'gated_entity.config';

  /**
   * GatedEntityHelper constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\gated_entity\Plugin\GatedEntityLockerManager $locker_manager
   *   The factory for configuration objects.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, ConfigFactoryInterface $config_factory, GatedEntityLockerManager $locker_manager) {
    $this->entityTypeManager = $entity_type_manager;
    $this->configFactory = $config_factory->get(static::GATED_ENTITY_CONFIG);
    $this->lockerManager = $locker_manager;
  }

  /**
   * Get allowed entity types.
   *
   * @return array
   *   Entity types.
   */
  public function getAllowedEntityTypes() {
    $entity_definitions = $this->entityTypeManager->getDefinitions();
    $allowed_types = array_intersect_key($entity_definitions, self::ALLOWED_ENTITY_TYPES);
    $types = [];
    foreach ($allowed_types as $type) {
      $types[$type->id()] = [
        'name' => $type->getLabel()->__toString(),
        'bundle_of' => $type->getBundleOf(),
      ];
    }
    return $types;
  }

  /**
   * Get default locker id.
   *
   * @return string
   *   Default locker id.
   */
  public function getDefaultLockerId() {
    return $this->configFactory->get('default_locker') ?? static::DEFAULT_LOCKER_ID;
  }

  /**
   * Get default locker.
   *
   * @return string
   *   Default locker.
   */
  public function getDefaultLocker() {
    return $this->getLockers(TRUE)[$this->getDefaultLockerId()] ?? [];
  }

  /**
   * Get allowed entities.
   *
   * @return array
   *   Entities.
   */
  public function getConfigurableEntities($type) {
    $instances = [];
    $entities = $this->entityTypeManager->getStorage($type)->loadMultiple();
    foreach ($entities as $entity) {
      $instances[$entity->id()] = $entity->label();
    }
    return $instances;
  }

  /**
   * Get type mapping.
   */
  private function getTypeMapping($type) {
    return array_search($type, self::ALLOWED_ENTITY_TYPES) ?? $type;
  }

  /**
   * Get configured entities.
   */
  private function getConfiguredEntities() {
    return $this->configFactory->get('entities');
  }

  /**
   * Check if it is gated entity or not.
   *
   * @return bool
   *   TRUE or FALSE.
   */
  public function isGatedEntity(EntityInterface $entity) {
    $entityType = '';
    if (method_exists($entity, 'getType')) {
      $entityType = $entity->getType();
    }
    $typeId = $entity->getEntityTypeId();
    $mappedTypeId = $this->getTypeMapping($typeId);
    $types = $this->getConfiguredEntities();
    if (isset($types[$mappedTypeId]) && array_search($entityType, $types[$mappedTypeId])) {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * Check if the gated entity is locked or not.
   *
   * @return bool
   *   TRUE or FALSE.
   */
  public function isGatedEntityLocked(EntityInterface $entity) {
    if ($this->isGatedEntity($entity)) {
      $locker = $this->getLockerByEntity($entity);
      return !$locker->checkAccess();
    }
    return FALSE;
  }

  /**
   * Get list of lockers.
   *
   * @return array
   *   List of lockers available.
   */
  public function getLockers($not_limited = FALSE) {
    $plugins = $this->lockerManager->getDefinitions();
    if ($not_limited) {
      return $plugins;
    }
    $plugin_list = [];
    foreach ($plugins as $key => $plugin) {
      $plugin_list[$key] = $plugin['label'];
    }
    return $plugin_list;
  }

  /**
   * Get locker for an entity.
   *
   * @return \Drupal\gated_entity\Plugin\GatedEntityLockerInterface
   *   The assigned locker.
   */
  public function getLockerByEntity(EntityInterface $entity) {
    $plugin_id = $this->getDefaultLockerId();
    return $this->lockerManager->createInstance($plugin_id);
  }

}
